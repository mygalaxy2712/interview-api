Rails.application.routes.draw do
  get "/onboarders", to: "onboarders#index"
  post "/onboarders", to: "onboarders#store"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end