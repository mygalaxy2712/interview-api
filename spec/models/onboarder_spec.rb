# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Onboarder, type: :model do
  describe 'validation tests' do
    let(:onboarder_params) do
      {
        employee_id: 1,
        company_id: 3,
        start_date: '2021-10-01',
        employee_updated_at: '2021-06-10 19:58:02.516471 +0700',
        department: 'AB',
        location: 'US'
      }
    end
    it 'should be success' do
      onboarder = Onboarder.new(onboarder_params)
      is_success = onboarder.save
      expect(is_success).to eq(true)
    end

    it 'should have 6 errors' do
      onboarder = Onboarder.new
      is_valid = onboarder.valid?
      expect(is_valid).to eq(false)
      expect(onboarder.errors.size).to eq(6)
    end

    it 'should have a uniqueness error' do
      Onboarder.create(onboarder_params)
      onboarder = Onboarder.new(onboarder_params)
      is_valid = onboarder.valid?
      expect(is_valid).to eq(false)
      expect(onboarder.errors.size).to eq(1)
    end
  end
end
