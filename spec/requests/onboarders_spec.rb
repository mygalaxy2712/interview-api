# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Onboarders', type: :request do
  describe 'POST /onboarders' do
    context 'exists an onboarder' do
      let(:onboarder_params) do
        {
          employee_id: 1,
          company_id: 1,
          start_date: '2021-10-01',
          employee_updated_at: '2021-06-10 19:58:02.516471 +0700',
          department: 'AB',
          location: 'US'
        }
      end
      before(:each) do
        Onboarder.create(onboarder_params)
      end
      it 'should have status 422 and 6 errors' do
        post '/onboarders', params: { onboarder: Onboarder.new.attributes }
        parsed_body = JSON.parse(response.body).deep_symbolize_keys
        expect(response).to have_http_status(422)
        expect(parsed_body[:errors]).to eq([
                                             "Employee updated at can't be blank",
                                             "Start date can't be blank",
                                             "Location can't be blank",
                                             "Department can't be blank",
                                             "Company can't be blank",
                                             "Employee can't be blank"
                                           ])
      end
      it 'should have status 422 with out of date params requested' do
        post '/onboarders',
             params: { onboarder: onboarder_params.merge({ employee_updated_at: '2021-06-10 19:00:02.516471 +0700' }) }
        parsed_body = JSON.parse(response.body).deep_symbolize_keys
        expect(response).to have_http_status(422)
        expect(parsed_body[:errors]).to eq(['The record requested is out of date'])
      end
    end
  end
end
