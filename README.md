# The Interview API
## Initialization
- Start docker
```
docker-compose up -d
```
- Init database
```
docker-compose run api rake db:create
docker-compose exec api bundle exec rake db:migrate
```
- Run Test
```
docker-compose exec api bundle exec rspec
```
## Endpoints
- GET: `http://127.0.0.1:3000/onboarders`
- POST: `http://127.0.0.1:3000/onboarders`

```
//Post params
{
    "onboarder": {
        "employee_id": 1,
        "company_id": 1,
        "start_date": "2020-01-01",
        //New field for the solution
        "employee_updated_at": "2020-9-01",
        "department": "AB",
        "location": "US"
    }
}
```
