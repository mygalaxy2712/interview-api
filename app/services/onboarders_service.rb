class OnboardersService
  class << self
    def store(onboarder_params)
      onboarder = OnboardersRepository
                  .find_or_initailize({ employee_id: onboarder_params[:employee_id], company_id: onboarder_params[:company_id] },
                                        onboarder_params)
      is_new = onboarder.new_record?
      unless is_new
        if !onboarder_params[:employee_updated_at] || (onboarder_params[:employee_updated_at].to_datetime < onboarder.employee_updated_at.to_datetime)
          raise DmlError, ['The record requested is out of date']
        end

        onboarder.update(onboarder_params)
      end
      raise DmlError, onboarder.errors.full_messages unless onboarder.valid?

      if is_new
        onboarder.save
      end

      onboarder
    end
  end
end