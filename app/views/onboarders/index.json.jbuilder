# frozen_string_literal: true

json.data do
  json.array! @onboarders do |onboarder|
    json.id onboarder.id
    json.employee_id onboarder.employee_id
    json.company_id onboarder.company_id
    json.start_date onboarder.start_date
    json.department onboarder.department
    json.location onboarder.location
  end
end
