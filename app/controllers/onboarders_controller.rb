class OnboardersController < ApplicationController
  def index
    @onboarders = Onboarder.all
  end

  def store
    @onboarder = OnboardersService.store(onboarder_params)
    render 'onboarders/store', formats: :json
  rescue DmlError => e
    @error_messages = e.messages
    render 'commons/error', formats: :json, status: e.status
  end

  private

  def onboarder_params
    params.require(:onboarder).permit(
      :employee_id,
      :company_id,
      :start_date,
      :department,
      :location,
      :employee_updated_at
    )
  end
end
