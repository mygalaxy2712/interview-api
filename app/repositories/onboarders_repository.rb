class OnboardersRepository
  class << self
    def find_or_initailize condition, init_param
      Onboarder.where(condition)
               .first_or_initialize(init_param)
    end
  end
end