class Onboarder < ApplicationRecord
  validates :employee_updated_at, presence: true
  validates :start_date, presence: true
  validates :location, presence: true
  validates :department, presence: true
  validates :company_id, presence: true
  validates :employee_id, presence: true, uniqueness: { scope: :company_id,
                                                        message: 'existed in the company' }
end
