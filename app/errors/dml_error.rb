class DmlError < StandardError
  attr_reader :status
  attr_reader :messages

  def initialize(messages = [])
    @status = 422
    @messages = messages
    super()
  end
end