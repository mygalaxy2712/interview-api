class CreateOnboarders < ActiveRecord::Migration[5.2]
  def change
    create_table :onboarders do |t|
      t.bigint :employee_id, null: false
      t.bigint :company_id, null: false
      t.date :start_date, null: false
      t.string :department, null: false
      t.string :location, null: false

      t.timestamps
    end
  end
end
