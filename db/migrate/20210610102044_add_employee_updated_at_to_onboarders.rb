class AddEmployeeUpdatedAtToOnboarders < ActiveRecord::Migration[5.2]
  def change
    add_column :onboarders, :employee_updated_at, :datetime, null: false
  end
end
